/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_individualassignment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_personal_ke_1_nomor_2 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic here
        //Date dt = new Date(1000000000000L);
        Date curDate = new Date();
        String birthDate = "10-1-1994";
        DateFormat formatTgl = new SimpleDateFormat("dd-MM-yyyy");
        Date tglLahir = (Date) formatTgl.parse(birthDate);
        
        long selisih = Math.abs(curDate.getTime() - tglLahir.getTime());
        long n = selisih / (24 * 60 * 60 * 1000);
        
        System.out.println("Selisih antara tanggal lahir dengan tanggal hari ini adalah: " + n + " hari");       

    }
    
}
