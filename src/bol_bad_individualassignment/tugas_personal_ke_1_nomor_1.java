/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_individualassignment;

import java.text.*;
import java.util.*;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_personal_ke_1_nomor_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       //Date dt = new Date(1000000000000L);
        Date curDate = new Date();
        long birthDate = Date.parse("1/10/1994");
        
        DateFormat[] dtformat = new DateFormat[6];
        dtformat[0] = DateFormat.getInstance();
        dtformat[1] = DateFormat.getDateInstance();
        dtformat[2] = DateFormat.getDateInstance(DateFormat.MEDIUM);
        dtformat[3] = DateFormat.getDateInstance(DateFormat.FULL);
        dtformat[4] = DateFormat.getDateInstance(DateFormat.LONG);
        dtformat[5] = DateFormat.getDateInstance(DateFormat.SHORT);
        
        System.out.println("Display for all format the current date:");
        for (DateFormat dateform : dtformat) {
             System.out.println(dateform.format(curDate));
        }
        System.out.println(" ");
        
        System.out.println("Display for all format the birth date:");
        for (DateFormat dateform : dtformat) {
            System.out.println(dateform.format(birthDate));
        }
    }
}
